package programing;
import java.util.Scanner;

public class CreaditCard_Validation {

    public static void main(String[] args) {
        System.out.println("Enter 11-digit credit card number:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();

        // Input validation
        if (input.length() != 11) {
            System.out.println("Invalid input. Please enter exactly 11 digits.");
            return;
        }

        int[] number = new int[11];
        for (int i = 0; i < number.length; i++) {
            number[i] = Character.getNumericValue(input.charAt(i));
        }

        // Luhn algorithm
        for (int i = 1; i < number.length; i += 2) {
            int doubledDigit = number[i] * 2;
            if (doubledDigit >= 10) {
                // Convert double-digit numbers to single digit by summing the digits
                doubledDigit = (doubledDigit % 10) + (doubledDigit / 10);
            }
            number[i] = doubledDigit; // Assign the doubled digit to the array
        }

        // Sum all digits
        int sum = 0;
        for (int num : number) {
            sum += num;
        }

        // Check validity
        if (sum % 10 == 0) {
            System.out.println("Credit Card Number Valid");
        } else {
            System.out.println("Credit Card Number Invalid");
        }
    }
}
