package miniProjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Xample {

	public static void main(String[] args) {
		
		try {
            // Provide path to the Excel file
			File file =new File("/home/kavitha/Documents/JAVA PROJECT/Untitled 1.xlsx");

            // Create an InputStream to read the Excel file
            FileInputStream inputStream = new FileInputStream(file);

            // Create a Workbook from the Excel file
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

            // Get the first sheet of the workbook

			 XSSFSheet sheet = workbook.getSheetAt(0);

            // Iterate over all the rows of the sheet
            for (Row row : sheet) {
                // Iterate over all the cells of the row
                for (Cell cell : row) {
                    // Print the cell value
                    System.out.print(cell.toString() + "\t");
                }
                // Move to the next line
                System.out.println();
            }

            // Close the workbook and release resources
            workbook.close();
            inputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	

	

}
