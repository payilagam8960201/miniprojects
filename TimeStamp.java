package miniProjects;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;
public class TimeStamp {

	
		

		

		    public static void main(String[] args) {
		        Path sourceDir = Paths.get("/home/kavitha/Documents/Photos");
		        Path destDir1 = Paths.get("/home/kavitha/Documents/Photos/Ooty");
		        Path destDir2 = Paths.get("/home/kavitha/Documents/Photos/Kodai");
		        String targetDate1="2024-04-16";
		        transferFiles(targetDate1, sourceDir, destDir1);
		        String targetDate2="2024-04-15";
		        transferFiles(targetDate2, sourceDir, destDir2);
		    }

		    public static void transferFiles(String targetDate, Path sourceDir, Path destDir) {
		        try {
		            Date target = new SimpleDateFormat("yyyy-MM-dd").parse(targetDate);
		            Files.walkFileTree(sourceDir, new SimpleFileVisitor<>() {
		                @Override
		                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		                    if (Files.isRegularFile(file) && isTargetDate(file, target) &&
		                            (file.toString().toLowerCase().endsWith(".jpg") || file.toString().toLowerCase().endsWith(".jpeg"))) {
		                        Files.move(file, destDir.resolve(file.getFileName()), StandardCopyOption.REPLACE_EXISTING);
		                        System.out.println("Moved: " + file + " to " + destDir.resolve(file.getFileName()));
		                    }
		                    return FileVisitResult.CONTINUE;
		                }
		            });
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    }

		    private static boolean isTargetDate(Path file, Date targetDate) throws IOException {
		        return new SimpleDateFormat("yyyy-MM-dd").format(new Date(Files.getLastModifiedTime(file).toMillis())).equals(new SimpleDateFormat("yyyy-MM-dd").format(targetDate));
		    }
		}
