package ipl_jdbc;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class PostgresJDBC_ipl {
    public static void main(String[] args) {
        // Declare resources outside the try block for proper closing in finally block
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            // Register PostgreSQL JDBC driver
            Class.forName("org.postgresql.Driver");

            // Connect to the database
            String url = "jdbc:postgresql://localhost:5432/ipl";
            String username = "postgres";
            String password = "KAVI";
            connection = DriverManager.getConnection(url, username, password);

            if (connection != null) {
                System.out.println("Connected to the PostgreSQL database!");

                // Prepare SQL statement with placeholders
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter team name : ");
                String team = sc.nextLine();
                System.out.println("Enter player name : ");
                String player = sc.nextLine();
                String sqlQuery = "SELECT * FROM " + team + " WHERE players = ?";
                preparedStatement = connection.prepareStatement(sqlQuery);
                preparedStatement.setString(1, player);

                // Execute the query
                resultSet = preparedStatement.executeQuery();

                // Process the result set
                while (resultSet.next()) {
                    System.out.println(resultSet.getString("players"));
                    System.out.println(resultSet.getInt("score"));
                }
            } else {
                System.out.println("Failed to make connection!");
            }
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC driver not found!");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("Connection failure!");
            e.printStackTrace();
        } finally {
            // Close resources in finally block to ensure they are always closed
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
